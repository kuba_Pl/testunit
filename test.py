import unittest as ut
from zadaniepython import Vector,Matrix,MyError
from math import sqrt
from cStringIO import StringIO
import sys
class testVector(ut.TestCase):
    def setUp(self):
        self.generate()

    def generate(self):
        v0=Vector([-1.,3.,2.])
        v1=Vector([0.,-2.,1.])
        v2=Vector([-1.,1.])
        v3=Vector([1.])
        v4=Vector([0.,2.])
        m0=Matrix([[1.,2.],[2.,0.]])
        m1=Matrix([[-1.,0.,2.],[3.,2.4,1.]])
        m2=Matrix([[1.,0,0],[0.,1.,0.],[0.,0.,1.]])
        m3=Matrix([[1.,-1.,1.],[-1.,1.,1.],[1.,1.,-1.]])
        self.vectors=[v0,v1,v2,v3,v4]
        self.matrixes=[m0,m1,m2,m3]
    def test_add_method_vector_return_correct_length(self):
            self.assertEqual(len(self.vectors[0].values),3)
            self.assertEqual(len(self.vectors[1].values),3)
            self.assertEqual(len(self.vectors[2].values),2)
            self.assertEqual(len(self.vectors[3].values),1)
    def test_add_method_vector_return_correct_add(self):
            temp=self.vectors[0]+self.vectors[1]
            vtest=[-1,1,3]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=self.vectors[4]+self.vectors[2]
            vtest=[-1,3]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
    def test_add_method_vector_return_correct_substract(self):
            temp=self.vectors[0]-self.vectors[1]
            vtest=[-1,5,1]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=self.vectors[4]-self.vectors[2]
            vtest=[1,1]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
    def test_add_method_vector_return_correct_mul_scalar(self):
            temp=self.vectors[0]*2
            vtest=[-2,6,4]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=self.vectors[2]*-1
            vtest=[1,-1]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=(1./2.)*self.vectors[3]
            vtest=[1./2]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=(1./2.)*self.vectors[1]
            vtest=[0,-1,1./2]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=0*self.vectors[1]
            vtest=[0,0,0]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
    def test_add_method_vector_return_correct_scalarmult_vector(self):   
            temp=self.vectors[1]*self.vectors[0]
            self.assertEqual(-4,temp)
            temp=self.vectors[2]*self.vectors[4]
            self.assertEqual(2,temp)
    def test_add_method_vector_return_correct_mult_matrix(self):
            temp=self.vectors[2]*self.matrixes[0]
            vtest=[1,-2]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
            temp=self.vectors[0]*self.matrixes[2]
            vtest=self.vectors[0].values
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
    def test_add_method_vector_return_correct_multcross_vector(self):
            temp=self.vectors[1].cross(self.vectors[0])
            vtest=[-7.0,-1.0,-2.0]
            for i in range(len(temp.values)):
                 self.assertEqual(vtest[i],temp.values[i])
    def test_add_method_vector_return_correct_magnitude_vector(self):
            vtest=[sqrt(14.),sqrt(5.),sqrt(2.),sqrt(1.),sqrt(4.)]
            for i in range(len(self.vectors)):
                 self.assertEqual(vtest[i],self.vectors[i].magnitude())
                 
    def test_add_method_matrix_return_correct_dim(self):
            mtest=[2,2]
            self.assertEqual(mtest,self.matrixes[0].__getDim__())
            mtest=[2,3]
            self.assertEqual(mtest,self.matrixes[1].__getDim__())
            mtest=[3,3]
            self.assertEqual(mtest,self.matrixes[2].__getDim__())
    def test_add_method_matrix_return_correct_add(self):
            temp=self.matrixes[2]+self.matrixes[3]
            mtest=[[2.0, -1.0, 1.0], [-1.0, 2.0, 1.0], [1.0, 1.0, 0.0]]
            self.assertEqual(temp.table,mtest)
    def test_add_method_matrix_return_correct_substract(self):
            temp=self.matrixes[2]-self.matrixes[3]
            mtest=[[0.0, 1.0, -1.0], [1.0, 0.0, -1.0], [-1.0, -1.0, 2.0]]
            self.assertEqual(temp.table,mtest)
    def test_add_method_matrix_return_correct_mult_scalar(self):
            temp=self.matrixes[1]*2
            mtest=[[-2.0, 0.0, 4.0], [6.0, 4.8, 2.0]]
            self.assertEqual(temp.table,mtest)
            temp=(1./2.)*self.matrixes[0]
            mtest=[[0.5, 1.0], [1.0, 0.0]]
            self.assertEqual(temp.table,mtest)
    def test_add_method_matrix_return_correct_mult_vector(self):
            temp=self.matrixes[3]*self.vectors[0]
            mtest=[-2.0, 6.0, 0.0]
            self.assertEqual(temp.values,mtest)
    def test_add_method_matrix_return_correct_mult_matrix(self):
            temp=self.matrixes[2]*self.matrixes[3]
            mtest=[[1.0, -1.0, 1.0], [-1.0, 1.0, 1.0], [1.0, 1.0, -1.0]]
            self.assertEqual(temp.table,mtest)
    def test_add_method_raise_exception(self):
            
            old_stdout = sys.stdout
            sys.stdout = mystdout = StringIO()
            #self.vectors[0]+self.vectors[2]
            #self.vectors[0]-self.vectors[2]
            self.vectors[0].cross(self.vectors[2])
            with self.assertRaises(MyError):
                self.vectors[0]*self.vectors[2]
                
            self.vectors[0]+self.matrixes[0]
            #self.vectors[0]-self.matrixes[0]
            self.matrixes[1]*self.vectors[1]
            with self.assertRaises(MyError):
                self.vectors[0]*self.matrixes[0]
                
            self.matrixes[1]*self.matrixes[0]
            self.matrixes[1]+self.matrixes[0]
            self.matrixes[1]-self.matrixes[0]
            
            sys.stdout = old_stdout
if __name__=='__main__':
        ut.main()
